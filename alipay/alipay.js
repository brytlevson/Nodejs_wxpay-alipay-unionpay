const AlipaySDK = require("alipay-sdk").default;  // 导入 SDK
// 引入 alipayFormData 构造函数，用来创建网站支付需要的 form 表单
const AlipayFormData = require('alipay-sdk/lib/form').default;


class ALIPAY{
    constructor(p) {
        this.alipay =  new AlipaySDK(p);
    }

    /**
     * 通知验签   小程序  红包等才能用到
     * @param {*} obj 对象
     */
    async checkNotify(obj) {
        const result = await this.alipay.checkNotifySign(obj)
        return result
    }

    /**
     * 创建订单
     * @param {object} goods 
     * @out_trade_no string 商户订单号。64 个字符以内的大小，仅支持字母、数字、下划线。需保证该参数在商户端不重复。
     * @total_amount int/float 订单总金额，单位为人民币（元），取值范围为 0.01~100000000.00，精确到小数点后两位。
     * @subject string 商品标题/交易标题/订单标题/订单关键字等。 注意：不可使用特殊字符，如 /，=，& 等。
     * @notify_url string 支付宝服务器主动通知商户服务器里指定的页面http/https路径  必须在公网 IP 上才能收到
     * @pack_params obj   选传   自定义商品信息参数 
     */
    async createOrder(goods) {  // 创建订单
        let method = 'alipay.trade.page.pay'; 
        let bizContent = {
          out_trade_no: goods.out_trade_no, 
          product_code: 'FAST_INSTANT_TRADE_PAY', 
          total_amount: goods.total_amount, 
          subject: goods.subject, 
          timeout_express: '5m',
          passback_params: JSON.stringify(goods.pack_params), 
        }
        const formData = new AlipayFormData(); 
        // formData.addField('returnUrl', 'https://127.0.0.1:3000/payresult')
        formData.addField('notifyUrl', goods.notify_url)
        formData.addField('bizContent', bizContent)
        const result = await this.alipay.exec(method, {}, {
          formData: formData
        })
        return result
      }

      /**
       * 查询订单
       * @param {obj} goods 
       * @out_trade_no string 商户订单号
       */
      async queryOrder(goods) {  // 查询订单
        let method = 'alipay.trade.query'
        let bizContent = {
          out_trade_no: goods.out_trade_no  
        }
        const formData = new AlipayFormData()
        formData.addField('bizContent', bizContent)
        const result = await this.alipay.exec(method, {}, {
          formData: formData
        })
        return result
      }

      /**
       * 支付宝退款
       * @param {obj} goods 
       * @out_trade_no string  商户订单号
       * @refund_amount price/int/float   需要退款的金额   最大长度11   该金额不能大于订单金额,单位为元，支持两位小数
       * @out_request_no string    退款标识（退款单号）   最大长度64  标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。 
       * (和微信退款方法差不多商户退款单号.同一退款单号多次请求只退一笔.)
       */
      async aliRefund(goods) {  // 支付宝退款
        let method = 'alipay.trade.refund'
        let bizContent = {
          out_trade_no:  goods.out_trade_no,
          refund_amount: goods.refund_amount,  // 退款金额
          out_request_no: goods.out_request_no  // 标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。
        }
        const formData = new AlipayFormData()
        formData.addField('bizContent', bizContent)
        const result = await this.alipay.exec(method, {}, {
          formData: formData
        })
        return result
      }

      /**
       * 
       * @param {obj} goods 
       * @out_trade_no string 商户订单号
       * @out_request_no string  退款标识（退款单号） 最大长度64  请求退款接口时，传入的退款请求号，如果在退款请求时未传入，则该值为创建交易时的外部交易号
       */
      async aliRefundQuery(goods) { //  支付宝退款结果查询
        let method = 'alipay.trade.fastpay.refund.query'
        let bizContent = {
          out_trade_no:  goods.out_trade_no,
          out_request_no: goods.out_request_no
        }
        const formData = new AlipayFormData()
        formData.addField('bizContent', bizContent)
        const result = await this.alipay.exec(method, {}, {
          formData: formData
        })
        return result
      }
    
}

exports = module.exports = ALIPAY;