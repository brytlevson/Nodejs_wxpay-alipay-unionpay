## node.js 微信支付接口说明

[官方文档](https://pay.weixin.qq.com/wiki/doc/api/H5_sl.php?chapter=1_1 "官方文档")

```javascript
const wxpay = new WXPAY({
  appid: "微信公众号appid",
  mch_id: "微信支付的商户号id",
  partner_key: "商户平台秘钥",
  pfx: fs.readFileSync("..."), // 微信支付证书的二进制文件
});
```

###### queryOrder 查询订单 -- return Promise

```javascript
wxpay.queryOrder("商户订单号"); // return Promise
```

    没有reject.
    如果不传入商户订单号,则返回undefined.
    如果网络错误,则返回网络错误代码.
    网络请求成功则返回微信支付的返回代码.

###### createOrder 创建订单 -- return Promise

```javascript
wxpay.createOrder({
  body: "商品描述. --支付页面中顶部中间显示的文字.",
  out_trade_no: "商户订单号.--最大长度32",
  total_fee: "支付金额(单位:分)",
  spbill_create_ip: "付款者ip", // 复杂网络环境,参考 获取浏览器指纹
  notify_url: "本次支付完成后的回调url",
  trade_type: "支付类型", // 参考 trade_type
});
```
    trade_type:
        JSAPI--JSAPI支付（或小程序支付）
        NATIVE--Native支付
        APP--app支付
        MWEB--H5支付

[获取浏览器指纹](https://pay.weixin.qq.com/wiki/doc/api/H5_sl.php?chapter=15_7&index=6 "获取浏览器指纹")

###### refundOrder 订单退款 -- return Promise

```javascript
wxpay.refundOrder({
  out_trade_no: "商户订单号.--最大长度32",
  total_fee: "支付金额(单位:分)",
  refund_fee: "退款总金额(单位:分)，只能为整数，可部分退款",
  notify_url: "本次支付完成后的回调url",
  out_refund_no: "商户退款单号.同一退款单号多次请求只退一笔.", // 参考 out_refund_no
});
```
    out_refund_no:参考官网详细说明

[申请退款](https://pay.weixin.qq.com/wiki/doc/api/H5_sl.php?chapter=9_4&index=4 "申请退款")

###### closeOrder 关闭你订单 -- return Promise

```javascript
wxpay.closeOrder("商户订单号");
```
