## node.js 银联支付接口说明
[官方文档](https://open.unionpay.com/tjweb/acproduct/APIList?apiservId=568&acpAPIId=740&bussType=1 "官方文档")
[官方文档](https://open.unionpay.com/tjweb/user/mchTest/param "官方文档")

###### 初始化 

```javascript
const express = require('express');
const fs = require('fs');
const path = require('path');
const Unionpay = require('./Unionpay')

// 配置均为测试环境，正式环境需要将.test去掉
const config = {
  frontUrl: 'https://blog.csdn.net/brytlevson/article/list/1', // 前台通知地址，前台返回商户结果时使用，例：https://xxx.xxx.com/xxx
  backUrl: 'https://blog.csdn.net/brytlevson/article/list/1', // 后台通知地址
  frontTransUrl: 'https://gateway.test.95516.com/gateway/api/frontTransReq.do', // 前台交易请求地址
  appTransUrl: 'https://gateway.test.95516.com/gateway/api/appTransReq.do', // APP交易请求地址
  backTransUrl: 'https://gateway.test.95516.com/gateway/api/backTransReq.do', // 后台交易请求地址
  cardTransUrl: 'https://gateway.test.95516.com/gateway/api/cardTransReq.do', // 后台交易请求地址(若为有卡交易配置该地址)：
  queryTransUrl: 'https://gateway.test.95516.com/gateway/api/queryTrans.do', // 单笔查询请求地址
  batchTransUrl: 'https://gateway.test.95516.com/gateway/api/batchTrans.do', // 批量查询请求地址
  TransUrl: 'https://filedownload.test.95516.com/', // 文件传输类交易地址
  merId: '777290058188630', // 测试商户号，已被批准加入银联互联网系统的商户代码
};   
```

```js
// 将证书解析，获取数据参数
let parser = new Unionpay();

// 获取到相应的 privateKey  certificate
const pfxPath = './acp_test_sign.pfx'
const password = '000000'
const result = parser.parseSignedDataFromPfx(pfxPath, password);
const { privateKey } = result;
const { certificate } = result;

// 获取到相应的certId
const certId = parser.parseCertData(certificate);

config.certId = certId;
config.privateKey = privateKey;
config.publicKey = certificate;
config.frontReqUrl = 'https://gateway.test.95516.com/gateway/api/frontTransReq.do';
```

```js
// 实例化Unionpay
const unionpay = new Unionpay(config);
```

###### createOrder 创建订单 -- return Promise

```javascript
unionpay.createOrder({  // result为html格式的文档，打开之后会自动跳转到支付页面，即支付成功
  out_trade_no: '12313131',  // 商户订单号 String(8，40)  仅支持数字和字母
  total_amount: '1000',  // 支付金额 单位是分 String(1,12)  仅支持数字。 单位为【分】，参数值不能带小数 
  options: '华为p40', // 其中options为附近信息，比如商品描述等，但是传入进reqReserved中的必须为字符串，并且长度有限定，此处只做大致演示
});
```

###### queryOrder 查询订单 -- return Promise

```javascript
unionpay.queryOrder({
    out_trade_no: "商户订单号",
    txnTime: '2020-12-23 09:23:23' // 订单提交到银联的时间
});
```
