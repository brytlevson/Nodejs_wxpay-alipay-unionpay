## node.js 支付宝支付接口说明

[官方文档](https://opendocs.alipay.com/apis/api_1/alipay.trade.page.pay "官方文档")

###### 初始化

```javascript
const fs = require('fs');
const path = require('path');
const ALIPAY = require('./alipay')
const alipay = new ALIPAY({
    appId: '2016110300790506',
    privateKey: fs.readFileSync(path.join(__dirname, './sandbox-pem/app_private_key.txt'), 'ascii'), // 应用私钥
    alipayPublicKey: fs.readFileSync(path.join(__dirname, './sandbox-pem/alipay_public_key.txt'), 'ascii'),// 支付宝公钥
    gateway: 'https://openapi.alipaydev.com/gateway.do', // 支付宝的应用网关，此时为沙箱环境的网关
    // gateway: 'https://openapi.alipay.com/gateway.do', // 支付宝的应用网关，此时为正式环境的网关
    charset:'utf-8',	// 字符集编码
    version:'1.0',		// 版本，默认 1.0
    signType:'RSA2'		// 秘钥的解码版本
});
```

###### createOrder 创建订单 -- return Promise

```javascript
alipay.createOrder({
  subject: "商品标题/交易标题/订单标题/订单关键字等。 注意：不可使用特殊字符，如 /，=，& 等。",
  out_trade_no: "商户订单号.--最大长度64",
  total_amount: "支付金额(单位:元)",
  notify_url: "支付宝服务器主动通知商户服务器里指定的页面http/https路径  必须在公网 IP 上才能收到",
  pack_params: "可以不传   自定义商品信息参数 "
});
```

###### queryOrder 查询订单 -- return Promise

```javascript
alipay.queryOrder({out_trade_no："商户订单号"}); 
```

###### refundOrder 订单退款 -- return Promise

```javascript
alipay.aliRefund({
  out_trade_no: "商户订单号.--最大长度64",
  refund_amount: "退款总金额(单位:元)，只能为整数，可部分退款",
  out_request_no: "商户退款单号  最大长度64  标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。"
});
```

###### refundOrder 退款查询 -- return Promise

```js
alipay.aliRefundQuery({
  out_trade_no: "商户订单号.--最大长度64",
  out_request_no: "商户退款单号  最大长度64  标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。"
});
```